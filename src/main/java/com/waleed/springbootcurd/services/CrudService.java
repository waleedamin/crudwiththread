package com.waleed.springbootcurd.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.waleed.springbootcurd.Entities.Employee;
import com.waleed.springbootcurd.repositories.EmployeeRepo;

@Service
public class CrudService {

	@Autowired
	EmployeeRepo emprep;

	public String Create(List<Employee> employee) {
		emprep.saveAll(employee);
			
		return "inserted";
	}

	public String update(Employee employee, int id) {
		Employee emp = emprep.getById(id);

		emp.setFirstname(employee.getFirstname());

		emp.setLastname(employee.getLastname());

		emp.setEmail(employee.getEmail());

		emp.setSalary(employee.getSalary());

		emp.setDepartment(employee.getDepartment());

		emprep.save(emp);	
		return "Updated";
	}


	public String delete( int id) {
		emprep.deleteById(id);
		return "Deleted";
	}

	public List<Employee> list() {

	List<Employee> employees = emprep.findAll();

		return employees;

	}

}
