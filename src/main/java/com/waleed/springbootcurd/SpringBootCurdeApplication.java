package com.waleed.springbootcurd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
//@EnableWebSecurity
@EnableJpaRepositories 
public class SpringBootCurdeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootCurdeApplication.class, args);
	}

}
