package com.waleed.springbootcurd.controllers;

import java.lang.Thread.State;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.waleed.springbootcurd.Entities.Employee;
import com.waleed.springbootcurd.services.CrudService;

@RestController
@RequestMapping("/api/crude")
public class CrudeController {
	List<Employee> list=null;
	@Autowired
	CrudService crs;
	
	@PostMapping("/createemployee")
    public ResponseEntity<Employee> add(@RequestBody List<Employee>  employee) {
        
        
       Thread t = new Thread() {
			@Override
			public void run() {  
				crs.Create(employee);				

			}
		};
		t.start();
        return new ResponseEntity<Employee>(HttpStatus.OK);
    }
	

	
		
		@GetMapping("/getemployees")
	    public ResponseEntity<?> list() {        

			
			
			
			Thread t = new Thread() {
				@Override
				public void run() {  
					list= crs.list();				

				}
			};
			t.start();
	        return new ResponseEntity<>(list, HttpStatus.OK);

	    }

}
